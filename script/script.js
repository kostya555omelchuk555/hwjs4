function performMathOperation() {
  let num1, num2, operator;

  do {
    num1 = parseFloat(prompt('Enter the first number:'));
  } while (isNaN(num1));

  do {
    num2 = parseFloat(prompt('Enter the second number:'));
  } while (isNaN(num2));

  do {
    operator = prompt('Enter the mathematical operator (+, -, *, /):');
  } while (!['+', '-', '*', '/'].includes(operator));

  let result;

  switch (operator) {
    case '+':
      result = num1 + num2;
      break;
    case '-':
      result = num1 - num2;
      break;
    case '*':
      result = num1 * num2;
      break;
    case '/':
      result = num1 / num2;
      break;
  }

  console.log(`The result of ${num1} ${operator} ${num2} is ${result}`);
}